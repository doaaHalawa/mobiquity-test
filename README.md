# Mobiquity cafeTownsend POC : Automation testing project to test cafeTownsend

### What is this repository for?

- Quick summary
  This test automation program is intended to cover all possible scenarios for cafeTownsend portal.

### features:

- **Automation tool** : Selenium webdriver.
- **Scripting language** : Java.
- **Scripting IDE** : Eclipse.
- **Testing Framework** : 
-   * cucumber to apply BDD. Please check "https://cucumber.io/docs/guides/10-minute-tutorial/" for more details about cucumber .
 -  * TestNg for reporting and has a lot of annotations used to organize tests please check https://testng.org/doc/ for more details. 
-   * Maven for  for projects build, dependency and documentation please check https://maven.apache.org/guides/index.html for more details.
- **Reporting** : TestNg.
- **Source Control**: Git with Bitbucket.
- **CI**: Jenkins.
- **Supported Browsers** : default chrome but other browsers are also supported by changing the browser name to firefox from 'testng.xml' file.


### How do I get set up?

### For Windows ###
- Install Eclipse
- Install Maven from https://maven.apache.org/download.cgi, and add to your path
- Install the Java 8 jdk from java
- Add a JAVA_HOME environment variable that points to your jdk install directory
- Install git
- In Eclipse, download TestNG plugin by: Go to Help -> Eclipse Marketplace, Type the text TestNG in Find text box > Click Go button, Click Install button at "TestNG for Eclipse" area, Follow prompts.
- In Eclipse, download Cucumber plugin by: Go to Help -> Eclipse Marketplace, Type the text Cucumber Eclipse Plugin in Find text box > Click Go button, Click Install button , Follow prompts.
- In Eclipse, download json tools plugin by: Go to Help -> Eclipse Marketplace, Type the text Json Tools in Find text box > Click Go button, Click Install button , Follow prompts.


### How to get project for the first time:

  - git clone https://doaaHalawa@bitbucket.org/doaaHalawa/mobiquity-test.git
  - Import the project as an existing maven project 
  - run this on project :  mvn clean install 
  
  
### How to run tests:

- From the project select testng.xml file.
- Right click on testng file > select run as TestNg Suite
- And this will run all feature files 

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Main Concepts

- I'm using page object model design pattern so you should keep the tests and element locators separately. This will keep the code clean and easy to understand and maintain. The Page Object approach makes automation framework in a testing programmer friendly, more durable and comprehensive.
- I Follow the [Git model](http://nvie.com/posts/a-successful-git-branching-model/) and [good practices](https://sethrobertson.github.io/GitBestPractices/), like **feature branches**, commit early and often, useful commit messages, clean history via rebase, **squash** etc.
- [Write Once, Test Everywhere](http://electronicdesign.com/embedded/java-write-once-test-everywhere) means the [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) tests here should be [SSOT](https://en.wikipedia.org/wiki/Single_source_of_truth) for all platforms we support (Web). It is important that tests should be platform agnostic, but the execution engine stays aware in the same time. Key feature here is the Automation [DSL](http://martinfowler.com/books/dsl.html) and a [Strategy pattern](https://gist.github.com/atsuya046/8534620). This can be achieved via Application driver layer. The DSL will encapsulate commonly used functions (e.g. login, navigation) and [web-element action wrappers]() (e.g. safe `wait_and_click`)
- Abstractions live longer than details, so when creating test logic, I invest in the abstractions, not the concrete implementation.
- A screen shot is added per fail.(Not completed yet)
- Favor Declarative (**WHAT**) -over- Imperative (**HOW**) BDD. It is almost always better to develop atomic steps and reuse them in more generic ones via [Calling steps from step definitions](https://github.com/cucumber/cucumber/wiki/Calling-Steps-from-Step-Definitions). Find out more on [Pushing how down](http://www.marcusoft.net/2013/04/PushTheHowDown.html).


### Code architecture

Framework's layers should be close to the following diagram:

```
				===========================
				|        feature files    |
				===========================
					|                    |
			=====================================
			| Step definitions (step_functions) |
			=====================================
					|
				===============================
				|            pages          |
				===============================
					|                        |
			=========================================
			|            Selenium Wrappers(core)    |
			| (facade, decorators, adapters, proxy) |
			=========================================
					|                         |
				==================    ===============
				|| Automation code  || webdriver_api |
				==================    ================
```

---

SELENIUM ARCHITECTURE HERE

### Test Scope

The scope of testing here is having a full coverage for the manual scenarios written below for both happy and negative scenarios.

<details>
<summary>View test plan:</summary>
## Test Plan

| TestID | Test Case       | Expected Result    | Actual Result | Result             | Related Comment |
| ------ | --------------- | ------------------ | ------------- | ------------------ | --------------- |
| 1      | Check something | what should happen | what happened | :white_check_mark: |

</details>
