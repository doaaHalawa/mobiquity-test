package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase
{
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(css = "input[ng-model='user.name']")
	private WebElement userName_Txtbox;
	
	@FindBy(css = "input[ng-model='user.password']")
	private WebElement password_Txtbox;

	@FindBy(css = "button[class='main-button']")
	private WebElement login_btn;
	
	@FindBy(id = "greetings")
	private WebElement greeting_Txt;
	
	public void login(String username , String password){
		userName_Txtbox.sendKeys(username);	
		password_Txtbox.sendKeys(password);
		login_btn.click();
	}
	
	public String getGreetingText(){
		return this.getElementText(greeting_Txt);
	}
}
