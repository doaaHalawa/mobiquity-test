package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateEmployeePage extends PageBase
{
	public CreateEmployeePage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(css = "input[ng-model='selectedEmployee.firstName']")
	private WebElement firstname_Txt;
	
	@FindBy(css = "input[ng-model='selectedEmployee.lastName']")
	private WebElement lastname_Txt;
	
	@FindBy(css = "input[ng-model='selectedEmployee.startDate']")
	private WebElement startDate_Txt;
	
	@FindBy(css = "input[ng-model='selectedEmployee.email']")
	private WebElement email_Txt;
	
	@FindBy(css = "button[class='main-button']")
	private WebElement AddAndEdit_Button;
    
    
    public void createEmployee(String firstName, String lastName, String startDate, String email) {
    	enterText(firstname_Txt,firstName);
    	enterText(lastname_Txt,lastName);
    	enterText(startDate_Txt,startDate);
    	enterText(email_Txt,email);
    	click(AddAndEdit_Button);
		}
 
    

    }
