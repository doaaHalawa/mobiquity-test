package pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageBase {

	protected WebDriver driver ; 
	public Select select ; 
	WebDriverWait wait;
	// create constructor 
	public PageBase(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public void click(WebElement element) 
	{
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	public void enterText(WebElement element, String text) 
	{
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
		element.clear();
		element.sendKeys(text);
		
	}
	public void clickAtIndex(List<WebElement> element, int index) 
	{
		 wait = new WebDriverWait(driver, 10);
		 wait.until(ExpectedConditions.elementToBeClickable(element.get(index)));
		 element.get(index).click();
	}
	public String getElementText(WebElement element) {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
		String elementText = element.getText();
		return elementText;
	}
	public List<String> getListOfElemetsText(WebElement element,String tagname) {
		List<String> textList = new ArrayList<String>();  
		List<WebElement> links = element.findElements(By.tagName(tagname));
		for (int i = 0; i < links.size(); i++)
		{
			textList.add(links.get(i).getText());
		}
		return textList;
	}
	
	public void clickOnElementFromListWithText(WebElement element,String tagname, String text) {  
		List<WebElement> links = element.findElements(By.tagName(tagname));
		for (int i = 0; i < links.size(); i++)
		{
			if(links.get(i).getText() == text) {
				click(links.get(i));
				break;
			}
		}
		
	}
	
	public void acceptWindowAlert() {  
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	public void clickOnLastElementInList(WebElement element,String tagname) {  
		List<WebElement> links = element.findElements(By.tagName(tagname));
				click(links.get(links.size()-1));		
	}
	
	public void  selectItemFromDDLUsingText(WebElement dropDownList , String text) {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(dropDownList));
		select = new Select(dropDownList);
		select.selectByVisibleText(text);
	}
	
	public String getSelectedItemTextFromDDl(WebElement dropDownList) {
		select = new Select(dropDownList);
		WebElement option = select.getFirstSelectedOption();
		String selectedItem = option.getText();
		return selectedItem;
	}
	
	public String getElementAttribute(WebElement element, String attribute) {
		String attributeValue = element.getAttribute(attribute);
		return attributeValue;
	}
	
	public void verifyURLContainsText(String Text)
	{
		Assert.assertTrue(driver.getCurrentUrl().contains(Text));
	}
	
	public void navigateToURL(String URL)
	{
		driver.navigate().to(URL);
	}
	
	public String getUniqueString() {
        return new SimpleDateFormat("yyyyMMdd.HHmmss.").format(new Date()) + String.valueOf(new Random().nextInt(100));
    }
}
