package pages;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class EmployeesPage extends PageBase
{
	public EmployeesPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id = "bAdd")
	private WebElement create_Button;
	
	@FindBy(id = "bEdit")
	private WebElement edit_Button;
	
	@FindBy(id = "employee-list")
	private WebElement employee_List;
    
	@FindBy(id = "bDelete")
	private WebElement delete_Button;
	
    public void clickOnCreateButton() {
    	click(create_Button);
		}

    public void selectEmployee() {
    	clickOnLastElementInList(employee_List, "li");
		}
    
    public void clickOnEditButton() {
    	click(edit_Button);
		}
    
    public void deleteEmployee() {
    	click(delete_Button);
    	acceptWindowAlert();
		}
    
    public void verifyNewEmployeeCreation(String employee) {
    	List<String> employeeListText = getListOfElemetsText(employee_List ,"li");
    	Assert.assertTrue(employeeListText.contains(employee));
		}

    }
