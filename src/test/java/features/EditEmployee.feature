
 	Feature: Edit Employee 
  Description: As a user I want Edit employee
    So that I can perform tasks related to it
    
  Background: Navigate to cafe site login and create new employee
   Given I navigate to cafe login page
   And I insert user credintials "validUsername" and "validPassword" and click login
   And Click on Create button
   And I Add employee details
    
   Scenario: Perform valid Edit employee scenario
		When I select employee from the list
		And I Click on Edit button
    And I Add employee details
	  Then Employee should be displayed in the list


 	  