
 	Feature: Login Page
  Description: As a user I want view login page
    So that I can perform tasks related to it
    
  Background: Navigate to cafe login page
   Given I navigate to cafe login page
   
   Scenario Outline: Perform valid login scenario
    When I insert user credintials "<Username>" and "<Password>" and click login
	  Then I should be logged in successfully 

  Examples:
 	  |Scenario             |Username       |Password      |
 	  |valid credintails    |validUsername  |validPassword |

 	  