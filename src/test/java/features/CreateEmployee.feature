
 	Feature: Create Employee Page
  Description: As a user I want view Create page
    So that I can perform tasks related to it
    
  Background: Navigate to cafe login page and login
   Given I navigate to cafe login page
   And I insert user credintials "validUsername" and "validPassword" and click login
   
   Scenario: Perform valid create employee scenario
    When Click on Create button
    And I Add employee details
	  Then Employee should be displayed in the list


 	  