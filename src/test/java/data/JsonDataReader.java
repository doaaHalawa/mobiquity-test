package data;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JsonDataReader 
{	
	String jsonData ;
	public String JsonReader(String data) throws IOException, ParseException 
	{
		String filePath = System.getProperty("user.dir")+"/src/test/java/data/TestData.json";

		File srcFile = new File(filePath); 

		JSONParser parser = new JSONParser(); 
		JSONArray jarray = (JSONArray)parser.parse(new FileReader(srcFile)); 

		for(Object jsonObj : jarray) 
		{
			JSONObject person = (JSONObject) jsonObj ; 
			jsonData =  (String) person.get(data); 
			System.out.println(jsonData);

		}
		return jsonData;

	}

}
