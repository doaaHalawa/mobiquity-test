package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.JsonDataReader;
import pages.*;
import tests.TestBase;

public class CreateEmployee extends TestBase{
   CreateEmployeePage createObject; 
   EmployeesPage employeesObject;
   JsonDataReader json ;
   String lastName ;
   String firstName;


   @When("^Click on Create button$")
   public void I_Click_On_Create_button() throws Throwable {
	   employeesObject = new EmployeesPage(driver);
	   employeesObject.clickOnCreateButton();
   }
   
   @When("^I Add employee details$")
   public void I_add_employee_details() throws Throwable {
	   json = new JsonDataReader();
	   createObject = new CreateEmployeePage(driver);
	   firstName = json.JsonReader("firstName");
	   lastName = createObject.getUniqueString();
	   createObject.createEmployee(firstName,lastName, json.JsonReader("startDate"), json.JsonReader("email"));
   }
   
   @Then("^Employee should be displayed in the list$")
   public void Employee_should_be_in_the_list() throws Throwable {
	   employeesObject = new EmployeesPage(driver);
	   employeesObject.verifyNewEmployeeCreation(firstName+ " " + lastName );
	   
	   //TearDown Step
	   employeesObject.selectEmployee();
	   employeesObject.deleteEmployee();
   }
   
}
