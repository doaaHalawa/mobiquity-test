package steps;

import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.JsonDataReader;
import pages.*;
import tests.TestBase;

public class Login extends TestBase{
   LoginPage loginObject; 

   JsonDataReader json ;
	@Given("^I navigate to cafe login page$")
	public void I_navigate_to_cafe_login_page() throws Throwable {
		loginObject = new pages.LoginPage(driver);
		json = new JsonDataReader();
		loginObject.navigateToURL(json.JsonReader("baseURL"));
	}
	
	@When("^I insert user credintials \"([^\"]*)\" and \"([^\"]*)\" and click login$")
	public void login(String username, String password) throws Throwable {
		loginObject = new pages.LoginPage(driver);
		json = new JsonDataReader();
		loginObject.login(json.JsonReader(username), json.JsonReader(password));
	}
	
	@Then("^I should be logged in successfully$")
	public void I_should_be_logged_in_successfully() throws Throwable {
		loginObject = new pages.LoginPage(driver);
		Assert.assertEquals(loginObject.getGreetingText(), "Hello Luke");
	}

}
