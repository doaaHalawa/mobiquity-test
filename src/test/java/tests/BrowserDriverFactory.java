package tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserDriverFactory {

	private ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	private String browser;
	public static String downloadPath = System.getProperty("user.dir") + "\\Downloads";

	public BrowserDriverFactory(String browser) {
		this.browser = browser.toLowerCase();
	}	
	
	public static FirefoxOptions firefoxOption() {
		FirefoxOptions option = new FirefoxOptions();
		option.addPreference("browser.download.folderList", 2);
		option.addPreference("browser.download.dir", downloadPath);
		option.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
		option.addPreference("browser.download.manager.showWhenStarting", false);
		option.addPreference("pdfjs.disabled", true);
		return option;
	}

	public static ChromeOptions chromeOption() {
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default.content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadPath);
		options.setExperimentalOption("prefs", chromePrefs);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return options;
	}

	public WebDriver createDriver() {
		System.out.println("Starting " + browser + " locally");
		
		// Creating driver
		switch (browser) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
			driver.set(new ChromeDriver(chromeOption()));
			break;

		case "firefox":
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/drivers/geckodriver.exe");
			driver.set(new FirefoxDriver(firefoxOption()));
			break;
		}

		return driver.get();
	}

	public WebDriver createDriverGrid() throws MalformedURLException {
		String hubUrl = "http://localhost:4444/wd/hub";
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		System.out.println("Starting " + browser + " on grid");

		// Creating driver
		switch (browser) {
		case "chrome":
			ChromeOptions chrome = new ChromeOptions();
			//path for chrome browser
			chrome.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
			capabilities.setCapability("moz:firefoxOptions", chrome);
			capabilities.setBrowserName(chrome.getBrowserName());
			
			break;

		case "firefox":
			FirefoxOptions firefox = new FirefoxOptions();
			capabilities.setBrowserName(firefox.getBrowserName());
			//path for firefox browser
			firefox.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
			capabilities.setCapability("moz:firefoxOptions", firefox);
			break;
		}

		try {
			driver.set(new RemoteWebDriver(new URL(hubUrl), capabilities));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return driver.get();
	}
	


}